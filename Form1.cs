using System;
using System.Security.Cryptography;
using System.Windows.Forms;
using Crypter64.Clases;//usar las clases de encriptacion y desencriptacion

namespace Crypter64
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        byte[] decrypted;//auxiliar para aes,3des
        string desencriptado;//auxiliar para aes
        private void btnEncriptar_Click(object sender, EventArgs e)
        {


            if (string.IsNullOrEmpty(txtEncriptar.Text))
            {
                MessageBox.Show("Debe ingresar texto a encriptar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string texto = txtEncriptar.Text;

                //ver si esta seleccionada la opcion de base64
                if (comboAlgoritmo.SelectedItem.Equals("Base64"))
                {
                    B64 b64 = new B64();
                    string encriptado = b64.Base64Encode(texto);
                    txtDesencriptar.Text = encriptado;
                    txtEncriptar.Clear();
                }
                else if (comboAlgoritmo.SelectedItem.Equals("AES"))
                {
                    using (var aes = new AesCryptoServiceProvider())
                    {
                        aes.GenerateIV();
                        aes.GenerateKey();
                        aes.Mode = CipherMode.CBC;
                        aes.Padding = PaddingMode.PKCS7;
                        texto = txtEncriptar.Text;
                        AES aes1 = new AES();
                        byte[] encrypted = aes1.AESCrypto(AES.Mode.ENCRYPT, aes, System.Text.Encoding.UTF8.GetBytes(texto));

                        txtDesencriptar.Text = BitConverter.ToString(encrypted).Replace("-", " ");
                        txtEncriptar.Clear();
                        decrypted = aes1.AESCrypto(AES.Mode.DECRYPT, aes, encrypted);
                        desencriptado = System.Text.Encoding.UTF8.GetString(decrypted);
                    }
                }
                else if (comboAlgoritmo.SelectedItem.Equals("3DES"))
                {
                    using  (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
                    {
                        _3DES des = new _3DES();
                        texto = txtEncriptar.Text;
                        byte[] encrypted =  des.Encrypt(texto, tdes.Key, tdes.IV);
                        txtEncriptar.Clear();
                        txtDesencriptar.Text = BitConverter.ToString(encrypted);
                        desencriptado = des.Decrypt(encrypted,tdes.Key,tdes.IV);
                    
                    }
                }

            }
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDesencriptar.Text))
            {
                MessageBox.Show("Debe ingresar texto a desencriptar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string encriptado = txtDesencriptar.Text;

                //ver si esta seleccionada la opcion de base64
                if (comboAlgoritmo.SelectedItem.Equals("Base64"))
                {
                    B64 b64 = new B64();
                    string texto = b64.Base64Decode(encriptado);
                    txtEncriptar.Text = texto;
                    txtDesencriptar.Clear();
                }
                else if (comboAlgoritmo.SelectedItem.Equals("AES"))
                {
                    txtEncriptar.Text = desencriptado;
                    txtDesencriptar.Clear();
                }
                else if (comboAlgoritmo.SelectedItem.Equals("3DES"))
                {
                    txtEncriptar.Text = desencriptado;
                    txtDesencriptar.Clear();
                }


            }
        }




        private void btnCerrar_Click(object sender, EventArgs e)
        {

            Application.Exit();
        }
    }
}
