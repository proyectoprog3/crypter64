using System.IO;
using System.Security.Cryptography;



namespace Crypter64.Clases
{

    class AES
    {

        
        public enum Mode
        {

            ENCRYPT,
            DECRYPT
        };

        public  byte[] AESCrypto(Mode mode, AesCryptoServiceProvider aes, byte[] message)
        {

            using (var memStream = new MemoryStream())
            {

                CryptoStream cryptoStream = null;

                if (mode == Mode.ENCRYPT)
                    cryptoStream = new CryptoStream(memStream, aes.CreateEncryptor(), CryptoStreamMode.Write);

                else if (mode == Mode.DECRYPT)
                    cryptoStream = new CryptoStream(memStream, aes.CreateDecryptor(), CryptoStreamMode.Write);

                if (cryptoStream == null)
                    return null;

                cryptoStream.Write(message, 0, message.Length);

                cryptoStream.FlushFinalBlock();

                return memStream.ToArray();

            }


        }
    }
}