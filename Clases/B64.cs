﻿using System;


namespace Crypter64.Clases
{
    //CLASE PARA BASE64
    class B64
    {
        public string Base64Encode(string str)
        {

            byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(str); //Convertir a UTF-8
            return Convert.ToBase64String(encbuff);
        }

        public string Base64Decode(string str)
        {
            byte[] decbuff = Convert.FromBase64String(str);
            return System.Text.Encoding.UTF8.GetString(decbuff);
        }

    }
}
