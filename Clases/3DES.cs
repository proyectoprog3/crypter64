using System.IO;
using System.Security.Cryptography;

namespace Crypter64.Clases
{
    class _3DES
    {
     public   byte[] Encrypt(string plainText, byte[] Key, byte[] IV)
        {
            byte[] encrypted;   using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
            {
               
                ICryptoTransform encryptor = tdes.CreateEncryptor(Key, IV);
              
                using (MemoryStream ms = new MemoryStream())
                {
                   
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        
                        using (StreamWriter sw = new StreamWriter(cs))
                            sw.Write(plainText);
                        encrypted = ms.ToArray();
                    }
                }
            }
            
            return encrypted;
        }

      public    string Decrypt(byte[] cipherText, byte[] Key, byte[] IV)
        {
            string plaintext = null;
            // Create TripleDESCryptoServiceProvider  
            using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
            {
                // Create a decryptor  
                ICryptoTransform decryptor = tdes.CreateDecryptor(Key, IV);
                // Create the streams used for decryption.  
                using (MemoryStream ms = new MemoryStream(cipherText))
                {
                    // Create crypto stream  
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        // Read crypto stream  
                        using (StreamReader reader = new StreamReader(cs))
                            plaintext = reader.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }
    }
}
