﻿namespace Crypter64
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtEncriptar = new System.Windows.Forms.TextBox();
            this.btnEncriptar = new System.Windows.Forms.Button();
            this.txtDesencriptar = new System.Windows.Forms.TextBox();
            this.btnDesencriptar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.comboAlgoritmo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // txtEncriptar
            // 
            this.txtEncriptar.Location = new System.Drawing.Point(41, 49);
            this.txtEncriptar.Multiline = true;
            this.txtEncriptar.Name = "txtEncriptar";
            this.txtEncriptar.Size = new System.Drawing.Size(168, 193);
            this.txtEncriptar.TabIndex = 0;
            // 
            // btnEncriptar
            // 
            this.btnEncriptar.BackColor = System.Drawing.Color.Navy;
            this.btnEncriptar.FlatAppearance.BorderSize = 0;
            this.btnEncriptar.ForeColor = System.Drawing.Color.White;
            this.btnEncriptar.Location = new System.Drawing.Point(99, 263);
            this.btnEncriptar.Name = "btnEncriptar";
            this.btnEncriptar.Size = new System.Drawing.Size(75, 32);
            this.btnEncriptar.TabIndex = 1;
            this.btnEncriptar.Text = "Encriptar";
            this.btnEncriptar.UseVisualStyleBackColor = false;
            this.btnEncriptar.Click += new System.EventHandler(this.btnEncriptar_Click);
            // 
            // txtDesencriptar
            // 
            this.txtDesencriptar.Location = new System.Drawing.Point(383, 49);
            this.txtDesencriptar.Multiline = true;
            this.txtDesencriptar.Name = "txtDesencriptar";
            this.txtDesencriptar.Size = new System.Drawing.Size(168, 193);
            this.txtDesencriptar.TabIndex = 2;
            // 
            // btnDesencriptar
            // 
            this.btnDesencriptar.BackColor = System.Drawing.Color.Navy;
            this.btnDesencriptar.FlatAppearance.BorderSize = 0;
            this.btnDesencriptar.ForeColor = System.Drawing.Color.White;
            this.btnDesencriptar.Location = new System.Drawing.Point(448, 263);
            this.btnDesencriptar.Name = "btnDesencriptar";
            this.btnDesencriptar.Size = new System.Drawing.Size(97, 32);
            this.btnDesencriptar.TabIndex = 3;
            this.btnDesencriptar.Text = "Desencriptar";
            this.btnDesencriptar.UseVisualStyleBackColor = false;
            this.btnDesencriptar.Click += new System.EventHandler(this.btnDesencriptar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.ErrorImage = null;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.Location = new System.Drawing.Point(625, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(16, 16);
            this.btnCerrar.TabIndex = 5;
            this.btnCerrar.TabStop = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // comboAlgoritmo
            // 
            this.comboAlgoritmo.FormattingEnabled = true;
            this.comboAlgoritmo.Items.AddRange(new object[] {
            "Base64",
            "AES",
            "3DES"});
            this.comboAlgoritmo.Location = new System.Drawing.Point(241, 148);
            this.comboAlgoritmo.Name = "comboAlgoritmo";
            this.comboAlgoritmo.Size = new System.Drawing.Size(121, 23);
            this.comboAlgoritmo.TabIndex = 6;
            this.comboAlgoritmo.Text = "Algoritmos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Primero seleccione un algoritmo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(640, 436);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboAlgoritmo);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnDesencriptar);
            this.Controls.Add(this.txtDesencriptar);
            this.Controls.Add(this.btnEncriptar);
            this.Controls.Add(this.txtEncriptar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crypter64";
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEncriptar;
        private System.Windows.Forms.Button btnEncriptar;
        private System.Windows.Forms.TextBox txtDesencriptar;
        private System.Windows.Forms.Button btnDesencriptar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.ComboBox comboAlgoritmo;
        private System.Windows.Forms.Label label1;
    }
}


